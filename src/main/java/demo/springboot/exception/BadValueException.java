package demo.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadValueException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public BadValueException(String message) {
		super(message);
	}
}
