package demo.springboot.controller;

import demo.springboot.entity.Task;
import demo.springboot.exception.BadValueException;
import demo.springboot.exception.ResourceNotFoundException;
import demo.springboot.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tasks")
public class TaskController {

	@Autowired
	private TaskRepository taskRepository;


	@PutMapping("/{id}")
	public Task updateUser(@RequestBody Task task, @PathVariable ("id") long id) {
		if(task.getDescription() == null){
			throw new BadValueException("task description is required");
		}
		Task existingTask = this.taskRepository.findById(id)
			.orElseThrow(() -> new ResourceNotFoundException("Cannot find the task with given id"));
		 existingTask.setDescription(task.getDescription());
		 existingTask.setPriority(task.getPriority());
		 return this.taskRepository.save(existingTask);
	}

}
